<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.agileai.domain.DataRow"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="com.agileai.util.DateUtil"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>日报记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="xheditor/xheditor-1.2.2.min.js"></script>
<script type="text/javascript" src="xheditor/xheditor_lang/zh-cn.js"></script>
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background-color: #FFFFFF;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ 元素/标签选择器 ~~ */
ul, ol, dl { /* 由于浏览器之间的差异，最佳做法是在列表中将填充和边距都设置为零。为了保持一致，您可以在此处指定需要的数值，也可以在列表所包含的列表项（LI、DT 和 DD）中指定需要的数值。请注意，除非编写一个更为具体的选择器，否则您在此处进行的设置将会层叠到 .nav 列表。 */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* 删除上边距可以解决边距会超出其包含的 div 的问题。剩余的下边距可以使 div 与后面的任何元素保持一定距离。 */
	padding-right: 15px;
	padding-left: 15px; /* 向 div 内的元素侧边（而不是 div 自身）添加填充可避免使用任何方框模型数学。此外，也可将具有侧边填充的嵌套 div 用作替代方法。 */
}
a img { /* 此选择器将删除某些浏览器中显示在图像周围的默认蓝色边框（当该图像包含在链接中时） */
	border: none;
}

/* ~~ 站点链接的样式必须保持此顺序，包括用于创建悬停效果的选择器组在内。 ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* 除非将链接设置成极为独特的外观样式，否则最好提供下划线，以便可从视觉上快速识别 */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* 此组选择器将为键盘导航者提供与鼠标使用者相同的悬停体验。 */
	text-decoration: none;
}

/* ~~ 此固定宽度容器包含所有其它 div ~~ */
.container {
	width: 100%;
	background-color: #FFF;
	overflow: hidden; /* 此声明可使 .container 了解其内部浮动列的结束位置以及包含列的位置 */
}

.sidebar1 {
	float: left;
	width: 16%;
	background-color: #D9E9Ff;
}
.content {
	padding:0;
	width: 99%;
	float: left;
}

/* ~~ 此分组的选择器为 .content 区域中的列表提供了空间 ~~ */


/* ~~ 导航列表样式（如果选择使用预先创建的 Spry 等弹出菜单，则可以删除此样式） ~~ */
ul.nav {
	list-style: none; /* 这将删除列表标记 */
	border-top: 1px solid #666; /* 这将为链接创建上边框 – 使用下边框将所有其它项放置在 LI 中 */
}
ul.nav li {
	border-bottom: 1px solid #666; /* 这将创建按钮间隔 */
}
ul.nav a, ul.nav a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	padding: 5px 5px 5px 15px;
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	text-decoration: none;
	background-color: #BEDAFF;
	color:gray;
}
ul.nav a:hover, ul.nav a:active, ul.nav a:focus { /* 这将更改鼠标和键盘导航的背景和文本颜色 */
	background-color: #579EFF;
	color: #FFF;
}
-->
</style>

<script type="text/javascript">
</script>

</head>
<body>
<div class="container" style="height:100%">
  <div class="content" style="height:100%;overflow:scroll;overflow-x:hidden">
   <%
   		List dayWorkRecords = (List) pageBean.getAttribute("dayWorkRecords");
   		Date currentDate = DateUtil.getDateTime(DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date()));
		dayWorkRecords = (List) pageBean.getAttribute("dayWorkRecords");
		pageBean.setRsList(dayWorkRecords);
		for (int i = 0; i < pageBean.getRsList().size(); i++) {
			DataRow row = (DataRow)pageBean.getRsList().get(i);
			Timestamp twTime = row.getTimestamp("TW_TIME");
			Date dateTemp = new Date(twTime.getTime());
			long dateDiff = DateUtil.getDateDiff(currentDate, dateTemp, DateUtil.DAY);
			String weekText = DateUtil.getWeekText(dateTemp);
      %>
    <div>
	<form id="dayworkForm<%=i%>" name="dayworkForm<%=i%>" method="post" action="">
	<%
	String NOW_TIME=pageBean.inputValue("NOW_TIME");
	String TW_TIME=pageBean.inputDate(i,"TW_TIME");
	%>
	
	<table cellspacing="0" cellpadding="0" style="width:100%;margin-left:2px;">
		<tr>
		<% 
		if(!"Auditer".equals(pageBean.inputValue("EMP_JOB"))){
		if(NOW_TIME.equals(TW_TIME)){
		%>
			<td class="header" style="background-color: #D9E9Ff">
			<%} else{ %>
			<td class="header" >
			<%} %>
			<span style="height: 30px;line-height:30px;">
			&nbsp;日期：<%=pageBean.inputDate(i,"TW_TIME")%>
			&nbsp;&nbsp;<%=weekText%>
			&nbsp;&nbsp;员工：<%=pageBean.inputValue(i,"USER_NAME")%>
	      	&nbsp;&nbsp;工作环境：
	      	<input id="TW_ENV_TYPE_<%=i%>" label="工作环境" name="TW_ENV_TYPE_<%=i%>" type="text" value="<%=pageBean.selectText("TW_ENV_TYPE",pageBean.inputValue(i,"TW_ENV"))%>" size="15"  class="text" readonly="readonly"/>
			<input id="TW_ENV_<%=i%>" label="工作环境" name="TW_ENV_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"TW_ENV")%>" />
          	<input type="hidden" name="TW_ID_<%=i%>" id="TW_ID_<%=i%>" value="<%=pageBean.inputValue(i,"TW_ID")%>" />	
          	<input type="hidden" id="USER_ID_<%=i%>" label="USER_ID_<%=i%>" name="USER_ID_<%=i%>" value="<%=pageBean.inputValue(i,"USER_ID")%>" />		
			</span>
      		</td>    	
		</tr>
		<tr>
	      <td>
          <textarea style="width:100%" name="TW_CONTENT" cols="45" rows="15" readonly="readonly" class="xheditor {skin:'o2007blue',tools:'Bold,Italic,Underline,Strikethrough,FontColor,BackColor,SelectAll,Removeformat,List,Outdent,Indent,Link,Unlink,Fullscreen'}"><%=pageBean.inputValue(i,"TW_CONTENT")%>
          </textarea>
          </td>
        </tr>
	</table>
	<input type="hidden" name="actionType" id="dayworkActionType<%=i%>" />
    </form>
    </div>
     <%
     }
     %>
     <%
     }
     %>
     
     <%
    	List noteRecords = (List) pageBean.getAttribute("noteRecords");
		pageBean.setRsList(noteRecords);
    	noteRecords = (List) pageBean.getAttribute("noteRecords");
		pageBean.setRsList(noteRecords);
		for (int i = 0; i < pageBean.getRsList().size(); i++) {
			DataRow row = (DataRow)pageBean.getRsList().get(i);
			String noteId = row.getString("NOTE_ID");
      %>
    <div>
	<form id="noteForm<%=i%>" name="noteForm<%=i%>" method="post" action="">
	<table cellspacing="0" cellpadding="0"  style="width:100%;margin-left:2px;">
		<tr>
			<td class="header">
			<span style="height: 30px;line-height:30px;">
			标题：<input type="text" name="NOTE_TITLE" id="NOTE_TITLE" value="<%=pageBean.inputDate(i,"NOTE_TITLE")%>" />
          	</span>
          	<div id="__ToolBar__" style="float:right;padding-right: 10px;">
			<input type="hidden" name="NOTE_ID" id="NOTE_ID" value="<%=pageBean.inputValue(i,"NOTE_ID")%>" />
			<input type="hidden" name="USER_ID" id="USER_ID" value="<%=pageBean.inputValue(i,"USER_ID")%>" />
			<input type="hidden" name="NOTE_SORT" id="NOTE_SORT" value="<%=pageBean.inputValue(i,"NOTE_SORT")%>" />			
			</div>  
			</td>
		</tr>
		<tr>
	      <td>
          <textarea style="width:99%" name="NOTE_DESCRIBE" cols="45" rows="17"  class="xheditor {skin:'o2007blue',tools:'Bold,Italic,Underline,Strikethrough,FontColor,BackColor,SelectAll,Removeformat,List,Outdent,Indent,Link,Unlink,Fullscreen'}"><%=pageBean.inputValue(i,"NOTE_DESCRIBE")%>
          </textarea>
          </td>
        </tr>
	</table>
	<input type="hidden" name="actionType" id="noteActionType<%=i%>" />
		<input type="hidden" name="rowIndex" id="rowIndex"/>	
    </form>
    </div>
     <%
     }
     %>     
<!-- end .content -->
	</div>
  <!-- end .container --></div>
<script language="javascript">
setRsIdTag('TW_ID');
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
