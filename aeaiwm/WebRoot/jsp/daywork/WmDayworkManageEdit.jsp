<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>日报记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function onchange_note(){
	$("#note_title").show();
	$(".dayworkTr").hide();
}
function onchange_daywork(){
	$("#note_title").hide();
	$(".dayworkTr").show();
	
}
function savaDayWork(){
	 postRequest("form1", {
			actionType : 'savaDayWork',
			onComplete : function(responseText) {
				if(responseText=='true'){
					parent.doSubmit({actionType:'prepareDisplay'});
				}else{
					alert(responseText);
				}
				
			}
		});	
} 
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
		<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">

</table>
</div>
<table>
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="savaDayWork()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent()"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
<table class="detailTable" cellspacing="0" cellpadding="0" id="msgTable">
<tr>
<th width="100" nowrap>工作类型</th>
<td>
<input id="daywork" type="radio" label="日报" name="daywork_note" checked="true" value="0" onclick="onchange_daywork()"/>日报
<input id="note" type="radio" label="备忘记录" name="daywork_note" value="1" onclick="onchange_note()"/>备忘记录
</td>
</tr>
<tr class="dayworkTr">
	<th width="100" nowrap>时间</th>
	<td><input id="TW_TIME" label="时间" name="TW_TIME" type="text" value="<%=pageBean.inputDate("TW_TIME")%>" size="10" class="text" style="width:100px;" /><img id="TW_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr class="dayworkTr">
	<th width="100" nowrap>工作环境</th>
	<td><select id="TW_ENV" label="工作环境" name="TW_ENV" class="select" style="width:100px;"><%=pageBean.selectValue("TW_ENV")%></select>
</td>
</tr>
<tr id="note_title" style="display:none;" >
	<th width="100" nowrap>备忘标题</th>
	<td>
	<input id="NOTE_TITLE" type="text" label="备忘标题" name="NOTE_TITLE" value="<%=pageBean.inputValue("NOTE_TITLE")%>"/>
	</td>
</tr>
</table>

<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="TW_ID" name="TW_ID" value="<%=pageBean.inputValue4DetailOrUpdate("TW_ID","")%>" />
<input type="hidden" id="NOTE_ID" name="NOTE_ID" value="<%=pageBean.inputValue4DetailOrUpdate("NOTE_ID","")%>" />
<input id="USER_ID" name="USER_ID" type="hidden" value="<%=pageBean.inputValue("USER_ID")%>" size="24" class="text" />
</form>
<script language="javascript">
initCalendar('TW_TIME','%Y-%m-%d','TW_TIMEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("TW_TIME");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
