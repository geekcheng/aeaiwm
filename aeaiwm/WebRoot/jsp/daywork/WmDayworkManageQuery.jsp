<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page language="java" import="com.agileai.hotweb.controller.core.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>日报记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function queryDayWork(){
	var startTime=$('#START_TIME').val();
	var endTime=$('#END_TIME').val();
	var content=$('#CONTENT').val();
	parent.queryDayWorks(startTime,endTime,content);
	parent.PopupBox.closeCurrent();
}
function conditionReset(){
	postRequest('form1',{actionType:'conditionReset',onComplete:function(responseText){
		if (responseText == 'success'){
			parent.doSubmit({actionType:'prepareDisplay'});			
		}else{
			alert('出错啦，请联系管理员！');
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0" id="msgTable">
<tr>
	<th width="100" nowrap>起始时间</th>
	<td><input id="START_TIME" label="起始时间" name="START_TIME" type="text" value="<%=pageBean.inputDate("START_TIME")%>" size="24" class="text" /><img id="ST_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>结束时间</th>
	<td><input id="END_TIME" label="结束时间" name="END_TIME" type="text" value="<%=pageBean.inputDate("END_TIME")%>" size="24" class="text" /><img id="ET_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
	<th width="100" nowrap>查询内容</th>
	<td><input id="CONTENT" label="结束时间" name="CONTENT" type="text" value="<%=pageBean.inputDate("CONTENT")%>"  size="24" class="text" />
</td>
</tr>
</table>
<table><tr>
<td style="padding-left:100px">
&nbsp;&nbsp;
<input  type="button" name="button" id="button" value="查询" class="formbutton" onclick="queryDayWork()" />
</td>
<td >
&nbsp;&nbsp;
<input  type="button" name="button" id="button" value="重置" class="formbutton" onclick="conditionReset()" />
</td>
<td>
&nbsp;&nbsp;
<input style="left: 500px" type="button" name="button" id="button" value="关闭" class="formbutton" onclick="parent.PopupBox.closeCurrent()" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="TW_ID" name="TW_ID" value="<%=pageBean.inputValue4DetailOrUpdate("TW_ID","")%>" />
<input type="hidden" id="NOTE_ID" name="NOTE_ID" value="<%=pageBean.inputValue4DetailOrUpdate("NOTE_ID","")%>" />
<input id="USER_ID" name="USER_ID" type="hidden" value="<%=pageBean.inputValue("USER_ID")%>" size="24" class="text" />
</form>
<script language="javascript">
initCalendar('START_TIME','%Y-%m-%d','ST_TIMEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("START_TIME");

initCalendar('END_TIME','%Y-%m-%d','ET_TIMEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("END_TIME");

initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
