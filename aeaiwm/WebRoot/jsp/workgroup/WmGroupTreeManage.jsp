<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>工作组管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doRefresh(nodeId){
	$('#GRP_ID').val(nodeId);
	doSubmit({actionType:'refresh'});
}
var targetTreeBox;
function showParentSelectBox(){
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标目录',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "WmGroupParentSelect";
	var url = 'index?'+handlerId+'&GRP_ID='+$("#GRP_ID").val();
	targetTreeBox.sendRequest(url);
}
function doChangeParent(){
	postRequest('form1',{actionType:'changeParent',onComplete:function(responseText){
		if (responseText == 'success'){
			doRefresh($('#GRP_ID').val());			
		}else {
			alert('迁移父节点出错啦！');
		}
	}});
}
function doSave(){
	if (checkSave()){
		$("#operaType").val('update');
		doSubmit({actionType:'save'});
	}
}
function checkSave(){
	var result = true;
	if (validation.checkNull($('#GRP_NAME').val())){
		writeErrorMsg($("#GRP_NAME").attr("label")+"不能为空!");
		selectOrFocus('GRP_NAME');
		return false;
	}
	if (validation.checkNull($('#GRP_TYPE').val())){
		writeErrorMsg($("#GRP_TYPE").attr("label")+"不能为空!");
		selectOrFocus('GRP_TYPE');
		return false;
	}
	if (!validation.checkDateTime($('#GRP_START_TIME').val())){
		writeErrorMsg($("#GRP_START_TIME").attr("label")+"日期/时间格式不正确!");
		selectOrFocus('GRP_START_TIME');
		return false;
	}
	if (isValid($('#GRP_END_TIME').val()) && !validation.checkDateTime($('#GRP_END_TIME').val())){
		writeErrorMsg($("#GRP_END_TIME").attr("label")+"日期/时间格式不正确!");
		selectOrFocus('GRP_END_TIME');
		return false;
	}
	return result;
}
function doMoveUp(){
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	doSubmit({actionType:'moveDown'});
}
function doCopyCurrent(){
	doSubmit({actionType:'copyCurrent'});
}
function doDelete(){
	if (confirm('确定要进行节点删除操做吗？')){
		doSubmit({actionType:'delete'});
	}
}
function doInsertChild(){
	if (checkInsertChild()){
		$("#operaType").val('insert');
		doSubmit({actionType:'insertChild'});
	}
}
function checkInsertChild(){
	var result = true;
if (validation.checkNull($('#CHILD_GRP_NAME').val())){
	writeErrorMsg($("#CHILD_GRP_NAME").attr("label")+"不能为空!");
	selectOrFocus('CHILD_GRP_NAME');
	return false;
}
if (validation.checkNull($('#CHILD_GRP_TYPE').val())){
	writeErrorMsg($("#CHILD_GRP_TYPE").attr("label")+"不能为空!");
	selectOrFocus('CHILD_GRP_TYPE');
	return false;
}
if (!validation.checkDateTime($('#CHILD_GRP_START_TIME').val())){
	writeErrorMsg($("#CHILD_GRP_START_TIME").attr("label")+"日期/时间格式不正确!");
	selectOrFocus('CHILD_GRP_START_TIME');
	return false;
}

if (validation.checkNull($('#CHILD_GRP_TYPE').val())){
	if (!validation.checkDateTime($('#CHILD_GRP_END_TIME').val())){
		writeErrorMsg($("#CHILD_GRP_END_TIME").attr("label")+"日期/时间格式不正确!");
		selectOrFocus('CHILD_GRP_END_TIME');
		return false;
	}
}
	return result;
}
function doCancel(){
	doRefresh($('#GRP_ID').val());
}
function changeTab(tabId){
	$('#currentTabId').val(tabId);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
	<div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;分组列表</h3>        
	<div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
	<%=pageBean.getStringValue("menuTreeSyntax")%></div>
    <b class="b9"></b>
    </div>
	</td>
	<td width="85%" valign="top">
<div class="photobg1" id="tabHeader">
    <div class="newarticle1" onclick="changeTab('0')">基本信息</div>
    <div class="newarticle1" onclick="changeTab('1')">关联用户</div>
</div>   
<div class="photobox newarticlebox" id="Layer0" style="height:427px;"> 
<div style="padding:0 5px 5px 5px; margin-top:9px;">
	    <div id="__ToolBar__" style="margin-top: 2px">
   		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doSave()"<%}%> class="bartdx" hotKey="E" align="center"><input id="saveImgBtn" value="&nbsp;" title="保存" type="button" class="saveImgBtn" style="margin-right:0px;" />保存</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doDelete()"<%}%> class="bartdx" hotKey="D" align="center"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" style="margin-right:0px;" />删除</td>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doCopyCurrent()"<%}%> class="bartdx" align="center"><input id="copyImgBtn" value="&nbsp;" title="复制" type="button" class="copyImgBtn" style="margin-right:0px;" />复制</td>    
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="showParentSelectBox()"<%}%> class="bartdx" align="center"><input id="moveImgBtn" value="&nbsp;" title="迁移" type="button" class="moveImgBtn" style="margin-right:0px;" />迁移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()"<%}%> class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()"<%}%> class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>
	    </tr></table></div>    
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>分组名称</th>
	<td><input id="GRP_NAME" label="分组名称" name="GRP_NAME" type="text" value="<%=pageBean.inputValue("GRP_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组类型</th>
	<td><select id="GRP_TYPE" label="分组类型" name="GRP_TYPE" class="select"><%=pageBean.selectValue("GRP_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>&nbsp;<%=pageBean.selectRadio("GRP_STATE")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>起始日期</th>
	<td><input id="GRP_START_TIME" label="起始日期" name="GRP_START_TIME" type="text" value="<%=pageBean.inputDate("GRP_START_TIME")%>" size="24" class="text" /><img id="GRP_START_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>终止日期</th>
	<td><input id="GRP_END_TIME" label="终止日期" name="GRP_END_TIME" type="text" value="<%=pageBean.inputDate("GRP_END_TIME")%>" size="24" class="text" /><img id="GRP_END_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
	    </table>
	</div>	
    
	<div style="margin-top: 10px;padding: 5px;">
	    <div id="__ToolBar__">
		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doInsertChild()"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:0px;" />新增</td>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCancel()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" style="margin-right:0px;" />取消</td>    
	    </tr></table>
	    </div>     
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>分组名称</th>
	<td><input id="CHILD_GRP_NAME" label="分组名称" name="CHILD_GRP_NAME" type="text" value="<%=pageBean.inputValue("CHILD_GRP_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组类型</th>
	<td><select id="CHILD_GRP_TYPE" label="分组类型" name="CHILD_GRP_TYPE" class="select"><%=pageBean.selectValue("CHILD_GRP_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>&nbsp;<%=pageBean.selectRadio("CHILD_GRP_STATE")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>起始日期</th>
	<td><input id="CHILD_GRP_START_TIME" label="起始日期" name="CHILD_GRP_START_TIME" type="text" value="<%=pageBean.inputDate("CHILD_GRP_START_TIME")%>" size="24" class="text" /><img id="CHILD_GRP_START_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>终止日期</th>
	<td><input id="CHILD_GRP_END_TIME" label="终止日期" name="CHILD_GRP_END_TIME" type="text" value="<%=pageBean.inputDate("CHILD_GRP_END_TIME")%>" size="24" class="text" /><img id="CHILD_GRP_END_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
	    </table>
	</div>        
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:427px;;display:none;overflow:hidden;">
<iframe id="UserFrame" src="index?GroupUserQueryList&grpId=<%=pageBean.inputValue("GRP_ID")%>" width="100%" height="430" frameborder="0" scrolling="no"></iframe>
</div>         
    </td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="GRP_ID" name="GRP_ID" value="<%=pageBean.inputValue("GRP_ID")%>" />
<input type="hidden" id="GRP_PID" name="GRP_PID" value="<%=pageBean.inputValue("GRP_PID")%>" />
<input type="hidden" id="GRP_NUMBER" name="GRP_NUMBER" value="<%=pageBean.inputValue("GRP_NUMBER")%>" />
<input type="hidden" id="currentTabId" name="currentTabId" value="<%=pageBean.inputValue("currentTabId")%>" />
</form>
</body>
</html>
<script language="javascript">
<%if(pageBean.getBoolValue("isRootNode")){%>
setImgDisabled('saveImgBtn',true);
setImgDisabled('delImgBtn',true);
setImgDisabled('copyImgBtn',true);
setImgDisabled('moveImgBtn',true);
setImgDisabled('upImgBtn',true);
setImgDisabled('downImgBtn',true);
<%}%>

var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(<%=pageBean.inputValue("currentTabId")%>);

$(window).load(function() {
	setTimeout(function(){
		resetTabHeight(70);
		resetTreeHeight(70);
	},1);	
});

</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
initCalendar('GRP_START_TIME','%Y-%m-%d','GRP_START_TIMEPicker');
initCalendar('GRP_END_TIME','%Y-%m-%d','GRP_END_TIMEPicker');
initCalendar('CHILD_GRP_START_TIME','%Y-%m-%d','CHILD_GRP_START_TIMEPicker');
initCalendar('CHILD_GRP_END_TIME','%Y-%m-%d','CHILD_GRP_END_TIMEPicker');
</script>
