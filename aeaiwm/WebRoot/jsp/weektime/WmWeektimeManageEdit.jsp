<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>周期定义</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>开始日期</th>
	<td><input id="WT_BEGIN" label="开始日期" name="WT_BEGIN" type="text" value="<%=pageBean.inputDate("WT_BEGIN")%>" size="24" class="text" /><img id="WT_BEGINPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>结束日期</th>
	<td><input id="WT_END" label="结束日期" name="WT_END" type="text" value="<%=pageBean.inputDate("WT_END")%>" size="24" class="text" /><img id="WT_ENDPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="WT_ID" name="WT_ID" value="<%=pageBean.inputValue4DetailOrUpdate("WT_ID","")%>" />
</form>
<script language="javascript">
initCalendar('WT_BEGIN','%Y-%m-%d','WT_BEGINPicker');
initCalendar('WT_END','%Y-%m-%d','WT_ENDPicker');
requiredValidator.add("WT_BEGIN");
datetimeValidators[0].set("yyyy-MM-dd").add("WT_BEGIN");
requiredValidator.add("WT_END");
datetimeValidators[1].set("yyyy-MM-dd").add("WT_END");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
