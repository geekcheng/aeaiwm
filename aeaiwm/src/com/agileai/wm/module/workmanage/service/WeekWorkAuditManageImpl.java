package com.agileai.wm.module.workmanage.service;

import java.util.Date;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseService;
import com.agileai.util.DateUtil;

public class WeekWorkAuditManageImpl
        extends BaseService
        implements WeekWorkAuditManage {
    public WeekWorkAuditManageImpl() {
        super();
    }

    @Override
    public List<DataRow> getEmpJobRecords(String userId,String groupId){
    	DataParam param = new DataParam("USER_ID",userId,"GRP_ID",groupId);
    	String statementId = "WmGroup.getEmpJobRecords";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    
	@Override
	public DataRow queryWeekRecord(String userId, String weekId) {
		DataParam param = new DataParam("userId",userId,"weekId",weekId);
		String statementId = this.sqlNameSpace+"."+"queryWeekRecord";
		return this.daoHelper.getRecord(statementId, param);
	}

	@Override
	public List<DataRow> queryEntryRecords(String userId,String weekId) {
		DataParam param = new DataParam("userId",userId,"weekId",weekId);
		String statementId = this.sqlNameSpace+"."+"queryEntryRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public List<DataRow> queryPrepareRecords(String userId,String weekId) {
		DataParam param = new DataParam("userId",userId,"weekId",weekId);
		String statementId = this.sqlNameSpace+"."+"queryPrepareRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public List<DataRow> getGroupRecords(String userId) {
		DataParam param = new DataParam("userId",userId);
		String statementId = "WmGroup.getGroupRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}
	public List<DataRow> queryEmpRecords(String grpId) {
		DataParam param = new DataParam("grpId",grpId);
		String statementId = "WmGroup.queryEmpRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}
	@Override
	public DataRow retrieveCurrentWeekRow() {
		DataRow result = null;
		Date date = new Date();
		Date currentWeekStartDay = DateUtil.getBeginOfWeek(date);
		Date lastWeekEndDay = DateUtil.getDateAdd(currentWeekStartDay,DateUtil.DAY, -1);
		DataParam param = new DataParam("lastWeekEndDay",lastWeekEndDay);
		String statementId = "WmWeektime.retrieveCurrentWeekIdRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}
	@Override
	public DataRow getWeekRow(String weekId) {
		DataRow result = null;
		DataParam param = new DataParam("WT_ID",weekId);
		String statementId = "WmWeektime.getRecord";
		result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public DataRow getBeforeWeekRow(String weekId) {
		DataRow result = null;
		DataParam param = new DataParam("WT_ID",weekId);
		String statementId = "WmWeektime.getBeforeWeekRow";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}

	@Override
	public DataRow getNextWeekRow(String weekId) {
		DataRow result = null;
		DataParam param = new DataParam("WT_ID",weekId);
		String statementId = "WmWeektime.getNextWeekRow";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}
	@Override
	public DataRow getGroupName(DataParam param) {
		DataRow result = null;
		String statementId = "WmWeektime.getGroupName";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}
	public List<DataRow> getUserList(DataParam param) {
		String statementId = "WmWeektime.getUserList";
		return this.daoHelper.queryRecords(statementId, param);
	}
	public List<DataRow> getWeekList(DataParam param) {
		String statementId = "WmWeektime.getWeekList";
		return this.daoHelper.queryRecords(statementId, param);
	}
	public List<DataRow> getWeekPrepareList(DataParam param) {
		String statementId = "WmWeektime.getWeekPrepareList";
		return this.daoHelper.queryRecords(statementId, param);
	}
	
	
}
