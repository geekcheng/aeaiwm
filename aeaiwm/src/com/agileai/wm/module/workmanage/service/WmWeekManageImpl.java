package com.agileai.wm.module.workmanage.service;

import java.util.*;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;
import com.agileai.util.ListUtil;
import com.agileai.wm.module.workmanage.service.WmWeekManage;

public class WmWeekManageImpl
        extends MasterSubServiceImpl
        implements WmWeekManage {
    public WmWeekManageImpl() {
        super();
    }

    public String[] getTableIds() {
        List<String> temp = new ArrayList<String>();
        temp.add("WmWeekentry");
        temp.add("WmPrepare");
        return temp.toArray(new String[] {  });
    }
    
    
    @Override
	public List<DataRow> getGroupRecords(String userId) {
		DataParam param = new DataParam("userId",userId);
		String statementId = "WmGroup.getGroupRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}
    
    public List<DataRow> findNofinishWeeks(String weekId,String userId) {
    	DataParam param = new DataParam("WT_ID",weekId,"USER_ID",userId);
    	String statementId = "WmWeek.findNoFinishWeeksRecord";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    
    public List<DataRow> findLastWeekPresRecord(String weekId,String userId) {
    	DataParam param = new DataParam("WT_ID",weekId,"USER_ID",userId);
    	String statementId = "WmWeek.findLastWeekPresRecord";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    
    @Override
    public List<DataRow> findEntry(String wwId){
    	DataParam param = new DataParam("WW_ID",wwId);
    	String statementId = "WmWeek.findWmWeekentryRecords";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    
    @Override
    public List<DataRow> findWeekWorkPres(String preId){
    	DataParam param = new DataParam("PRE_ID",preId);
    	String statementId = "WmWeek.findWeekWorkPresRecord";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    
    @Override
    public List<DataRow> checkWeekTime(DataParam param){
    	String statementId = "WmWeek.checkWeekTime";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    public List<DataRow> getPersonalWmWeek(DataParam param){
    	String statementId = "WmWeek.getPersonalWmWeek";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    public List<DataRow> getWeekWork(DataParam param){
    	String statementId = "WmWeek.getWeekWork";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    
    public List<DataRow> insertWeekWorkEntry(List<DataRow> newEntry){
    	DataParam param = new DataParam("ENTRY_DESCRIBE",newEntry.get(0).getString("PRE_DESCRIBE"),
    			                        "ENTRY_ID",newEntry.get(0).getString("ENTRY_ID"),
    			                        "ENTRY_PLAN",newEntry.get(0).getString("PRE_LOAD"),
    			                        "WW_ID",newEntry.get(0).getString("WW_ID"),
    			                        "ENTRY_STATE",newEntry.get(0).getString("ENTRY_STATE"),
    			                        "ENTRY_FINISH",newEntry.get(0).getString("ENTRY_FINISH"),
    									"ENTRY_SORT",newEntry.get(0).getInt("ENTRY_SORT"));
    	String statementId = "WmWeek.insertWeekWorkEntryRecord";
    	return this.daoHelper.queryRecords(statementId, param);
    }
    
    
    @Override
	public DataRow getBeforeWeekRow(String weekId) {
		DataRow result = null;
		DataParam param = new DataParam("WT_ID",weekId);
		String statementId = "WmWeektime.getBeforeWeekRow";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			result = records.get(0);
		}
		return result;
	}
    
    public DataRow findMaxEntrySort(String wwId) {
    	DataRow result = null;
    	DataParam param = new DataParam("WW_ID",wwId);
    	String statementId ="WmWeek.findMaxEntrySort";
    	processDataType(param, tableName);
		result=this.daoHelper.getRecord(statementId, param);
		return result;
    }
    
    public DataRow findMaxPreSort(String wwId) {
    	DataRow result = null;
    	DataParam param = new DataParam("WW_ID",wwId);
    	String statementId ="WmWeek.findMaxPreSort";
    	processDataType(param, tableName);
		result=this.daoHelper.getRecord(statementId, param);
		return result;
    }
	@Override
	public void updateStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateStateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public void updateEntryState(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateEntryStateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public String findPower(String userId) {
		String power=null;
    	DataParam param = new DataParam("USER_ID",userId);		
		String statementId = sqlNameSpace+"."+"findUserPower";
		processDataType(param, tableName);
		this.daoHelper.queryRecords(statementId, param);
		return power;
	}

	public List<DataRow> insertWeekWorkEntryParam(List<DataRow> newEntryList){
    	DataParam param = new DataParam("ENTRY_DESCRIBE",newEntryList.get(0).getString("ENTRY_DESCRIBE"),
    			                        "ENTRY_ID",newEntryList.get(0).getString("ENTRY_ID"),
    			                        "ENTRY_PLAN",newEntryList.get(0).getString("ENTRY_PLAN"),
    			                        "WW_ID",newEntryList.get(0).getString("WW_ID"),
    			                        "ENTRY_STATE",newEntryList.get(0).getString("ENTRY_STATE"),
    			                        "ENTRY_FINISH",newEntryList.get(0).getString("ENTRY_FINISH"),
    			                        "ENTRY_GROUP",newEntryList.get(0).getString("ENTRY_GROUP"),
    									"ENTRY_SORT",newEntryList.get(0).getInt("ENTRY_SORT"));
    	String statementId = "WmWeek.insertWeekWorkEntryParam";
    	return this.daoHelper.queryRecords(statementId, param);
    }

	public List<DataRow> insertWeekWorkPrepareParam(List<DataRow> newEntryList) {
		DataParam param = new DataParam("PRE_ID",newEntryList.get(0).getString("PRE_ID"),
						                "WW_ID",newEntryList.get(0).getString("WW_ID"),
						                "PRE_DESCRIBE",newEntryList.get(0).getString("PRE_DESCRIBE"),
						                "PRE_LOAD",newEntryList.get(0).getString("PRE_LOAD"),
						                "PRE_SORT",newEntryList.get(0).getInt("PRE_SORT"));
		String statementId = "WmWeek.insertWeekWorkPrepareParam";
		return this.daoHelper.queryRecords(statementId, param);
	}
	
	@Override
    public List<DataRow> findWeekWorkEntry(String entryId){
    	DataParam param = new DataParam("ENTRY_ID",entryId);
    	String statementId = "WmWeek.findWeekWorkEntryRecord";
    	return this.daoHelper.queryRecords(statementId, param);
    }
	
	 public List<DataRow> insertWeekWorkPrepare(List<DataRow> newEntry){
    	DataParam param = new DataParam("PRE_ID",newEntry.get(0).getString("PRE_ID"),
    			                        "WW_ID",newEntry.get(0).getString("WW_ID"),
    			                        "PRE_DESCRIBE",newEntry.get(0).getString("PRE_DESCRIBE"),
    			                        "PRE_LOAD",newEntry.get(0).getString("PRE_LOAD"),
    			                        "PRE_SORT",newEntry.get(0).getInt("PRE_SORT"));
    	String statementId = "WmWeek.insertWeekWorkPrepareRecord";
    	return this.daoHelper.queryRecords(statementId, param);
	    }

	public DataRow findGroupRecord(DataParam param) {
		DataRow result = null;
    	String statementId ="WmWeek.findGroupRecord";
		result=this.daoHelper.getRecord(statementId, param);
		return result;
	}

    public List<DataRow> getEmpJobRecords(String userId,String groupId){
    	DataParam param = new DataParam("USER_ID",userId,"GRP_ID",groupId);
    	String statementId = "WmGroup.getEmpJobRecords";
    	return this.daoHelper.queryRecords(statementId, param);
    }

	public void insertWWPrepareParam(List<DataParam> params) {
		String statementId = "WmWeek.insertWWPrepareRecords";
		this.daoHelper.batchInsert(statementId, params);
	}
	public void insertWWEntryParam(List<DataParam> params) {
		String statementId = "WmWeek.insertWWEntryParams";
		this.daoHelper.batchInsert(statementId, params);
	}
}
