package com.agileai.wm.module.workgroup.handler;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.QueryModelListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.ListUtil;
import com.agileai.wm.module.workgroup.service.WmGroupTreeManage;

public class GroupUserQueryListHandler
        extends QueryModelListHandler {
    public GroupUserQueryListHandler() {
        super();
    }

    protected void processPageAttributes(DataParam param) {
        initMappingItem("EMP_JOB_TYPE",
                        FormSelectFactory.create("EMP_JOB_TYPE").getContent());
    }
    @PageAction
    public ViewRenderer addUserGroupRelation(DataParam param){
    	String grpId = param.get("grpId");
    	String userIds = param.get("userIds");
    	List<String> userIdList = new ArrayList<String>();
    	ListUtil.addArrayToList(userIdList, userIds.split(","));
    	getService().addRelation(grpId, userIdList);
    	return this.prepareDisplay(param);
    }
    @PageAction
    public ViewRenderer delUserGroupelation(DataParam param){
    	String grpId = param.get("grpId");
    	String userId = param.get("USER_ID");
    	getService().removeRelation(grpId, userId);
    	return this.prepareDisplay(param);
    } 
    protected void initParameters(DataParam param) {
        initParamItem(param, "roleId", "");
    }

    protected WmGroupTreeManage getService() {
        return (WmGroupTreeManage) this.lookupService(WmGroupTreeManage.class);
    }
}
