package com.agileai.wm.module.weektime.handler;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.wm.module.weektime.service.WmWeektimeManage;

public class WmWeektimeManageEditHandler
        extends StandardEditHandler {
    public WmWeektimeManageEditHandler() {
        super();
        this.listHandlerClass = WmWeektimeManageListHandler.class;
        this.serviceId = buildServiceId(WmWeektimeManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected WmWeektimeManage getService() {
        return (WmWeektimeManage) this.lookupService(this.getServiceId());
    }
}

