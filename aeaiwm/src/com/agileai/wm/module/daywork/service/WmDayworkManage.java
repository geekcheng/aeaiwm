package com.agileai.wm.module.daywork.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface WmDayworkManage
        extends StandardService {
	public DataRow getLastDayRecord(String currentUserId);
	public void saveDayWorkRecord(DataParam param); 
	
	public List<DataRow> findDayWorkRecords(DataParam param);
	public List<DataRow> findNoteRecords(DataParam param);
	
	public List<DataRow> getMngGroupRecords(String userId);
	public List<DataRow> queryDayEmpRecords(String grpId);
	
	public DataRow getDayRow(String twtime);
	public DataRow getUserRecord(DataParam param);
	public void updateUserRecord(DataParam param); 
	
	
	public String getBeforeDayRow(String twtime);
	public String getNextDayRow(String twtime);
	public String retrieveCurrentDayRow(String twtime);
	
	
}
