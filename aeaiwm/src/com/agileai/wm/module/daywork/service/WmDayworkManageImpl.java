package com.agileai.wm.module.daywork.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.util.DateUtil;

public class WmDayworkManageImpl
        extends StandardServiceImpl
        implements WmDayworkManage {
    public WmDayworkManageImpl() {
        super();
    }

	@Override
	public DataRow getLastDayRecord(String currentUserId) {
		DataParam param = new DataParam();
		param.put("currentUserId",currentUserId);
		String statementId = sqlNameSpace+"."+"getLastDayRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public void saveDayWorkRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"saveDayWorkRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public List<DataRow> findDayWorkRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findDayWorkRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> findNoteRecords(DataParam param) {
		String statementId = "WmNote.findNoteRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> getMngGroupRecords(String userId) {
		DataParam param = new DataParam("userId",userId);
		String statementId = "WmGroup.getMngGroupRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public List<DataRow> queryDayEmpRecords(String grpId) {
		DataParam param = new DataParam("grpId",grpId);
		String statementId = "WmGroup.queryEmpRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public DataRow getDayRow(String twtime) {
		DataRow result = null;
		DataParam param = new DataParam("TW_TIME",twtime);
		String statementId = "WmDaywork.getRecord";
		result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
	public DataRow getUserRecord(DataParam param) {
		DataRow result = null;
		String statementId = "WmDaywork.getUserRecord";
		result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
	public void updateUserRecord(DataParam param){
		String statementId = sqlNameSpace+"."+"updateUserRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
	

	public String getBeforeDayRow(String twtime) {
		Date selectDate = DateUtil.getDate(twtime);
		Date beforeDate = DateUtil.getDateAdd(selectDate, DateUtil.DAY, -1);
		twtime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,beforeDate);
		return twtime;
	}

	@Override
	public String getNextDayRow(String twtime) {
		Date selectDate = DateUtil.getDate(twtime);
		Date nextDate = DateUtil.getDateAdd(selectDate, DateUtil.DAY, +1);
		twtime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,nextDate);
		return twtime;
	}

	@Override
	public String retrieveCurrentDayRow(String twtime) {
		SimpleDateFormat currentTime = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		twtime = currentTime.format(new Date());// new Date()为获取当前系统时间
		return twtime;
	}

}
